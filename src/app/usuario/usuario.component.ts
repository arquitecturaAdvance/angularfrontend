import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { UsuarioService } from '../usuario/usuario.service';
import { UtilitiesService } from '../utilities.service';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UsuarioComponent implements OnInit {
  path = "/usuarios"

  @ViewChild(MatPaginator) paginator: MatPaginator;

  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }
  usuario :any = {id: null, nombre:"", apellido:"", nombre_usuario:"", password: "" }
  dataSource : any;
  // dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['id', 'nombre', 'apellido', 'nombre_usuario'];
  
  public unidades :any;

  constructor(private usuarioService: UsuarioService, private utilitiesService: UtilitiesService) {
   }

  ngOnInit() {
    this.get();
  }

  post(){
    this.utilitiesService.post(this.usuario, this.path).subscribe(resp=>{
      console.log(resp)
      this.get()
    })
  }

  update(data:any){
    this.utilitiesService.update(data, this.path)
  }

  delete(data:any){
    this.utilitiesService.delete(data, this.path).subscribe(resp=>{
      console.log(resp)
      this.get()
    })
    
  }

  get() :void{
    this.utilitiesService.get(this.path).subscribe(data=>{
        this.dataSource = new MatTableDataSource<any>(data);
        this.dataSource.paginator = this.paginator;

    })
  }

}
