import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent }      from './header/header.component';
import { FooterComponent }      from './footer/footer.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ProfesorComponent } from './profesor/profesor.component';




const routes: Routes = [
  // { path: 'header', component: HeaderComponent },
  { path: 'usuario', component: UsuarioComponent },
  { path: 'profesor', component: ProfesorComponent },
  { path: '', redirectTo: '/recaudacion', pathMatch: 'full' },
];




@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}