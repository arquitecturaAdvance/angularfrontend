import { Component, OnInit, ChangeDetectorRef, OnDestroy, } from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {UtilitiesService} from '../utilities.service';
import { ActivatedRoute, Router  } from '@angular/router';
// import { timingSafeEqual } from 'crypto';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})


export class HeaderComponent implements OnInit {

  step = null;
  routes: any = [
      {name: "usuario", subMenu:[{name:"usuario"}]},
      {name: "profesor", subMenu:[{name:"profesor"}]},
  ];
  
    mobileQuery: MediaQueryList;
    fillerNav = [];
    icon;
    fillerContent = Array.from({length: 3}, () =>
        `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
         labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
         laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
         voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
         cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);
  
    private _mobileQueryListener: () => void;
  
    constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private utilitiesService : UtilitiesService, private route: Router  ) {
      this.mobileQuery = media.matchMedia('(max-width: 600px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
    }
    
  
    ngOnDestroy(): void {
      this.mobileQuery.removeListener(this._mobileQueryListener);
    }
  
    shouldRun = true;
  ngOnInit() {
    console.log(this.routes[0].name)
    this.icon = Array.from({length: this.routes.length}, (_, i) => false);
    this.fillerNav = Array.from({length: this.routes.length}, (_, i) => `${this.utilitiesService.capitalizeFirstLetter(this.routes[i].name)}`);
  }

  setStep(index: number) {
    this.step = index;
  }

  setIcon(i){
    this.icon[i] = !this.icon[i];
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  show():boolean{
    // if(this.route.url === "/recaudacion" || this.route.url === "/unidadNegocio" || this.route.url === "/empresa" )
    return false
  }


}
