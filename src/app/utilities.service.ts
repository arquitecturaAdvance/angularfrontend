import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {
  url: String = "http://localhost:3000";

  constructor(private http: HttpClient,) { }
  
  capitalizeFirstLetter(s :string) : string {
    return s.charAt(0).toUpperCase() + s.slice(1);
  }

  getUrl() :String{
    return this.url;
  }

  post(data: any, path: String){
    return this.http.post(this.url+""+path+".json", data)
  }

  update(data: any, path: String){
    return this.http.put(this.url+""+path+"/"+data.id+".json", data).subscribe(resp=>{
      console.log(resp)
    })
  }

  delete(data: any, path: String){
    return this.http.delete(this.url+""+path+"/"+data.id+".json", data)
  }

  get (path: String): Observable<any[]> {
    return this.http.get<any>(this.url+""+path+".json")
  }
  
}
