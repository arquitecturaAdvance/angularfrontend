import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { UtilitiesService } from '../utilities.service';

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html',
  styleUrls: ['./profesor.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ProfesorComponent implements OnInit {
  path = "/profesors"

  @ViewChild(MatPaginator) paginator: MatPaginator;

  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  usuarios :any[];

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }
  profesor :any = {id: null, nombre:"", usuario_id:""}
  dataSource : any;
  displayedColumns: string[] = ['id', 'nombre', 'usuario_id'];
  
  public unidades :any;

  constructor(private utilitiesService: UtilitiesService) {
   }

  ngOnInit() {
    this.get();
    this.getUsuarios();
  }

  getUsuarios() :void{
    this.utilitiesService.get("/usuarios").subscribe(data=>{
      this.usuarios = data;
    })
  }

  getNombreUsuario(usuario_id) :any{
    try {
      return this.usuarios.find(x=>x.id == usuario_id).nombre
    } catch (error) {
      return ""
    }
  }

  post(){
    this.utilitiesService.post(this.profesor, this.path).subscribe(resp=>{
      console.log(resp)
      this.get()
    })
  }

  update(data:any){
    this.utilitiesService.update(data, this.path)
  }

  delete(data:any){
    this.utilitiesService.delete(data, this.path).subscribe(resp=>{
      console.log(resp)
      this.get()
    })
    
  }

  get() :void{
    this.utilitiesService.get(this.path).subscribe(data=>{
        this.dataSource = new MatTableDataSource<any>(data);
        this.dataSource.paginator = this.paginator;

    })
  }

}
